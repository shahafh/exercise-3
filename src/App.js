import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import store from "./store";
import NewItem from "./NewItem";
import AllItems from "./AllItems";
import "./App.css";

const App = () => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route path="/all-items">
          <AllItems />
        </Route>
        <Route path="/">
          <NewItem />
        </Route>
      </Switch>
    </Router>
  </Provider>
);

export default App;
