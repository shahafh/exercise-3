import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import store, { itemsActions } from "../store";
import "../App.css";

const { compIds } = require("./enums");

const initialFields = () => {
  document.getElementById(compIds.NAME_INPUT).value = "";
  //document.getElementById(compIds.DATA_INPUT).value = 100;
  //document.getElementById(compIds.POST_INPUT).value = 100;
  //document.getElementById(compIds.GET_INPUT).value = 100;
};

const AddBtn = ({ state, add }) => {
  const onAdd = () => {
    const name = document.getElementById(compIds.NAME_INPUT).value;
    if (!name) {
      alert("please enter item name!");
      return;
    }
    add({ ...state, name: name });

    console.log(store.getState());

    alert("Item name " + name + " was Added!");
    initialFields();
  };

  return (
    <button className="addBtn" onClick={onAdd}>
      ADD ITEM
    </button>
  );
};

AddBtn.propTypes = {
  state: PropTypes.object.isRequired,
  add: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  items: state
});

export default connect(mapStateToProps, itemsActions)(AddBtn);
