import React from "react";
import PropTypes from "prop-types";
import "../App.css";

const AllItemsBtn = ({ onAllItems }) => (
  <button className="allItemsBtn" onClick={onAllItems}>
    ALL ITEMS
  </button>
);

AllItemsBtn.propTypes = {
  onAllItems: PropTypes.func.isRequired
};

export default AllItemsBtn;