import React from "react";
import PropTypes from "prop-types";
import "../App.css";

const { compIds, lineNames } = require("./enums");

const UnitInput = ({ id, onInput, enableInput, inputValue }) => {
  let unitStr = "TB";

  if (id.includes("get") || id.includes("post")) {
    unitStr = "Million";
  }

  return (
    <form className="inputContainer" action="/action_page.php">
      {enableInput ? (
        <input
          type="number"
          className="numInput"
          id={id}
          name="number"
          min="0"
          defaultValue="100"
          step="25"
          onInput={onInput}
        ></input>
      ) : (
        <input
          type="number"
          className="numInput"
          id={id}
          name="number"
          min="0"
          defaultValue={inputValue}
          step="25"
          disabled
        ></input>
      )}
      <div className="inputUnit">
        <span className="numUnit"> {unitStr}</span>
      </div>
    </form>
  );
};

UnitInput.propTypes = {
  id: PropTypes.string.isRequired,
  onInput: PropTypes.func.isRequired
};

const DataRow = ({ state, name, id, onInput, enableInput }) => {
  let costA = 0;
  let costB = 0;
  let inputValue = 0;

  switch (name) {
    case lineNames.DATA: {
      costA = state.dataCostA;
      costB = state.dataCostB;
      inputValue = state.dataUnit;
      break;
    }

    case lineNames.POST: {
      costA = state.postCostA;
      costB = state.postCostB;
      inputValue = state.postUnit;
      break;
    }
    case lineNames.GET: {
      costA = state.getCostA;
      costB = state.getCostB;
      inputValue = state.getUnit;
      break;
    }
    default: {
    }
  }

  return (
    <tr>
      <td className="leftCol">{name}</td>
      <td>
        <UnitInput
          id={id}
          onInput={onInput}
          enableInput={enableInput}
          inputValue={inputValue}
        />
      </td>
      <td>${costA}</td>
      <td>${costB}</td>
    </tr>
  );
};

DataRow.propTypes = {
  state: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onInput: PropTypes.func.isRequired
};

const TotalsRow = ({ state }) => (
  <tr>
    <td className="leftCol lastRow">Totals</td>
    <td className="lastRow">
      Savings: ${state.savings}{" "}
      <span className="percentageText">({state.percentage}%)</span>
    </td>
    <td className="lastRow">${state.totalA}</td>
    <td className="lastRow">${state.totalB}</td>
  </tr>
);

TotalsRow.propTypes = {
  state: PropTypes.object.isRequired
};

const Table = ({
  state,
  onDataInput,
  onPostInput,
  onGetInput,
  enableInput
}) => {
  return (
    <table className="resourceTable" cellSpacing="0" cellPadding="0">
      <tr className="tableHead">
        <td className="leftCol">Resource</td>
        <td>Unit</td>
        <td>A</td>
        <td>B</td>
      </tr>
      <DataRow
        state={state}
        name={"Data"}
        id={compIds.DATA_INPUT}
        onInput={onDataInput}
        enableInput={enableInput}
      />
      <DataRow
        state={state}
        name={"Post"}
        id={compIds.POST_INPUT}
        onInput={onPostInput}
        enableInput={enableInput}
      />
      <DataRow
        state={state}
        name={"Get"}
        id={compIds.GET_INPUT}
        onInput={onGetInput}
        enableInput={enableInput}
      />
      <tr className="emptyRow">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <TotalsRow state={state} />
    </table>
  );
};

Table.propTypes = {
  state: PropTypes.object.isRequired,
  onDataInput: PropTypes.func.isRequired,
  onPostInput: PropTypes.func.isRequired,
  onGetInput: PropTypes.func.isRequired
};

export default Table;
