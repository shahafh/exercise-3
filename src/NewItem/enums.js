const compIds = {
  DATA_INPUT: "dataInput",
  POST_INPUT: "postInput",
  GET_INPUT: "getInput",
  NAME_INPUT: "nameInput"
};

const lineNames = {
  DATA: "Data",
  POST: "Post",
  GET: "Get"
};

const costs = {
  COST_TB_A: 52,
  COST_TB_B: 43,
  COST_POST_A: 11,
  COST_POST_B: 16,
  COST_GET_A: 28,
  COST_GET_B: 27
};

const _compIds = compIds;
export { _compIds as compIds };
const _lineNames = lineNames;
export { _lineNames as lineNames };
const _costs = costs;
export { _costs as costs };
