import React from "react";
import { Link } from "react-router-dom";
import Table from "./Table";
import AddBtn from "./AddBtn";
import "../App.css";

const { compIds, costs } = require("./enums");

class NewItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      dataUnit: 100,
      postUnit: 100,
      getUnit: 100
    };

    this.state["dataCostA"] = this.state.dataUnit * costs.COST_TB_A;
    this.state["postCostA"] = this.state.postUnit * costs.COST_POST_A;
    this.state["getCostA"] = this.state.getUnit * costs.COST_GET_A;
    this.state["totalA"] =
      this.state.dataCostA + this.state.postCostA + this.state.getCostA;

    this.state["dataCostB"] = this.state.dataUnit * costs.COST_TB_B;
    this.state["postCostB"] = this.state.postUnit * costs.COST_POST_B;
    this.state["getCostB"] = this.state.getUnit * costs.COST_GET_B;
    this.state["totalB"] =
      this.state.dataCostB + this.state.postCostB + this.state.getCostB;

    this.state["savings"] = Math.abs(this.state.totalA - this.state.totalB);
    this.state["percentage"] =
      this.state.totalA >= this.state.totalB
        ? ((this.state.savings / this.state.totalA) * 100).toFixed(1)
        : ((this.state.savings / this.state.totalB) * 100).toFixed(1);

    this.onDataInput = this.onDataInput.bind(this);
    this.onPostInput = this.onPostInput.bind(this);
    this.onGetInput = this.onGetInput.bind(this);
  }

  onDataInput() {
    const dataUnit = Number(document.getElementById(compIds.DATA_INPUT).value);
    const dataCostA = dataUnit * costs.COST_TB_A;
    const dataCostB = dataUnit * costs.COST_TB_B;
    const totalA = dataCostA + this.state.postCostA + this.state.getCostA;
    const totalB = dataCostB + this.state.postCostB + this.state.getCostB;
    const savings = Math.abs(totalA - totalB);
    const percentage =
      totalA >= totalB
        ? ((savings / totalA) * 100).toFixed(1)
        : ((savings / totalB) * 100).toFixed(1);

    this.setState({
      dataUnit: dataUnit,
      dataCostA: dataCostA,
      dataCostB: dataCostB,
      totalA: totalA,
      totalB: totalB,
      savings: savings,
      percentage: percentage
    });
  }

  onPostInput() {
    const postUnit = Number(document.getElementById(compIds.POST_INPUT).value);
    const postCostA = postUnit * costs.COST_POST_A;
    const postCostB = postUnit * costs.COST_POST_B;
    const totalA = postCostA + this.state.dataCostA + this.state.getCostA;
    const totalB = postCostB + this.state.dataCostB + this.state.getCostB;
    const savings = Math.abs(totalA - totalB);
    const percentage =
      totalA >= totalB
        ? ((savings / totalA) * 100).toFixed(1)
        : ((savings / totalB) * 100).toFixed(1);

    this.setState({
      postUnit: postUnit,
      postCostA: postCostA,
      postCostB: postCostB,
      totalA: totalA,
      totalB: totalB,
      savings: savings,
      percentage: percentage
    });
  }

  onGetInput() {
    const getUnit = Number(document.getElementById(compIds.GET_INPUT).value);
    const getCostA = getUnit * costs.COST_GET_A;
    const getCostB = getUnit * costs.COST_GET_B;
    const totalA = getCostA + this.state.dataCostA + this.state.postCostA;
    const totalB = getCostB + this.state.dataCostB + this.state.postCostB;
    const savings = Math.abs(totalA - totalB);
    const percentage =
      totalA >= totalB
        ? ((savings / totalA) * 100).toFixed(1)
        : ((savings / totalB) * 100).toFixed(1);

    this.setState({
      getUnit: getUnit,
      getCostA: getCostA,
      getCostB: getCostB,
      totalA: totalA,
      totalB: totalB,
      savings: savings,
      percentage: percentage
    });
  }

  onAllItems() {}

  handleSubmit(e) {
    e.preventDefault();
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="headerTitle">A vs B</h1>
          <span>Cost Calculator</span>
        </header>

        <div className="bodyContainer">
          <div className="bodyContent">
            <form
              className=""
              action="/action_page.php"
              onSubmit={this.handleSubmit}
            >
              <span>{"Item Name: "}</span>
              <input
                id={compIds.NAME_INPUT}
                type="text"
                className=""
                placeholder="enter item name"
                autoFocus
              />
            </form>
          </div>

          <div className="tableContainer bodyContent">
            <Table
              state={this.state}
              onDataInput={this.onDataInput}
              onPostInput={this.onPostInput}
              onGetInput={this.onGetInput}
              enableInput={true}
            />
          </div>

          <div className="bodyContent">
            <AddBtn state={this.state} />
            <Link to="/all-items">All Items</Link>
          </div>
        </div>
      </div>
    );
  }
}

export default NewItem;
