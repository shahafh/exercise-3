import React from "react";
import PropTypes from "prop-types";
import Table from "../NewItem/Table";

const Details = ({ item }) => {
  return <Table state={item} enableInput={false} />;
};

class Line extends React.PureComponent {
  constructor({ id, item, onRemove, onEdit }) {
    super({ id, item, onRemove, onEdit });

    this.state = {
      isExpand: false
    };

    this.onExpand = this.onExpand.bind(this);
  }

  onEditClick() {
    /*TBD*/
  }

  onExpand() {
    this.setState({
      isExpand: !this.state.isExpand
    });
  }

  render() {
    return (
      <li className="itemLine">

          <button className="lineContent expandItem" onClick={this.onExpand}>
            >
          </button>

          <label className="lineContent">{this.props.item.name + " "}</label>
          {this.state.isExpand && <Details item={this.props.item} />}
          <button
            className="removeItem"
            onClick={() => this.props.onRemove(this.props.id)}
          >
            X
          </button>
          <button className="editItem" onClick={this.onEditClick}>
            edit
          </button>

      </li>
    );
  }
}

export default Line;
