import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { itemsActions } from "../store";
import Line from "./Line";
import "./AllItems.css";

const AllItems = ({ items, remove, edit }) => {
  return (
    <div className="App">
      <header className="App-header">
        <h1 className="headerTitle">All Items</h1>
        <span>Cost Calculator</span>
      </header>

      <div className="bodyContainer">
        <ul className="allItemsList">
          {items.map(obj => (
            <Line
              key={obj.id}
              id={obj.id}
              item={obj.item}
              onRemove={remove}
              onEdit={edit}
            />
          ))}
        </ul>

        <Link to="/">New Item</Link>
      </div>
    </div>
  );
};

Line.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      item: PropTypes.object.isRequired
    })
  ),
  remove: PropTypes.func.isRequired,
  edit: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  items: state
});

export default connect(mapStateToProps, itemsActions)(AllItems);
