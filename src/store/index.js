import { configureStore } from "@reduxjs/toolkit";
import ItemsReducer from "./rootReducer";

const store = configureStore({
  reducer: ItemsReducer.reducer
});

export default store;
export const itemsActions = ItemsReducer.actions;
