import { createSlice } from "@reduxjs/toolkit";
import { v4 } from "uuid";

const initialState = [
  { //structure
    id: "111",
    item: {
      name: "item1",
      dataUnit: 125,
      postUnit: 100,
      getUnit: 158,
      dataCostA: 1000,
      postCostA: 1100,
      getCostA: 1200,
      totalA: 3300,
      dataCostB: 1200,
      postCostB: 1350,
      getCostB: 1400,
      totalB: 4500,
      savings: 1500,
      percentage: 5.5
    }
  },
  { //structure
    id: "222",
    item: {
      name: "item2",
      dataUnit: 85,
      postUnit: 100,
      getUnit: 94,
      dataCostA: 1000,
      postCostA: 2222,
      getCostA: 1200,
      totalA: 3300,
      dataCostB: 1200,
      postCostB: 1234,
      getCostB: 1400,
      totalB: 4500,
      savings: 1500,
      percentage: 7.5
    }
  }
];

const ItemsReducer = createSlice({
  name: "allItems",
  initialState,
  reducers: {
    add: {
      reducer: (state, { payload }) => state.concat(payload),
      prepare: payload => ({
        payload: {
          id: v4(),
          item: payload
        }
      })
    },
    remove: (state, { payload }) => state.filter(item => item.id !== payload),
    edit: {
      reducer: (state, { payload }) =>
      state.map(item =>
        item.id === payload.id ? { ...item, isDone: !item.isDone } : item //TBDDD
      )
    }
  }
});

export default ItemsReducer;
